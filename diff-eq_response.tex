
\documentclass[11]{article}
\usepackage{spverbatim}
\usepackage[margin=1in]{geometry}
\usepackage{listings,amsmath,amsfonts}
\usepackage{xcolor}
\newcommand{\note}[1]{\textup{{\color{red}#1}}}
\newcommand{\C}{\mathbb{C}}

\definecolor{light-gray}{gray}{0.95}

\lstset{basicstyle=\color{blue}\ttfamily,
breaklines=true,
columns=fullflexible,
breakindent=0em,
aboveskip=3ex,
belowskip=3ex,
backgroundcolor=\color{light-gray},
keepspaces=true}
\setlength{\parindent}{0cm}

\newcommand{\add}[1]{{\color{cyan}{#1}}}
\usepackage{soul}

\begin{document}


% The {lstlisting} environment is like {verbatim} but more customizable (and has word wrapping)
% \end{lstlisting} Response to referee \begin{lstlisting}

We thank the referee for their assessment of the paper and their extensive and detailed comments.  We have revised the manuscript to address all the issues that were raised, as outlined below. To more easily identify changes in the manuscript, additions are shown in \add{cyan} and removed text is indicated by \st{strikethrough}.

\begin{lstlisting}
Reviewer #1:
This paper presents a quantum algorithm that "solves" a system of time-dependent ODEs (in the sense that it produces a quantum state that encodes the solution in superposition) exponentially faster than is possible for classical algorithms, and with exponentially improved error dependence compared with previous quantum algorithms. The algorithm is based on the use of spectral methods to produce a linear system that can be solved using a previously known quantum algorithm. In order to prove the desired bound on the algorithm's complexity, the authors need to carry out some quite technical analysis of the condition number and other characteristics of this linear system.

The authors' work can be seen as a direct generalization of prior work on efficient Hamiltonian simulation algorithms. However, the authors need to do quite a lot of work to prove their results, as well as coming up with some new ideas. I expect that their algorithm will be useful for practical applications. I read through all of the proofs (not all in exhaustive detail), and did not spot any significant technical issues (although note some minor points below). The paper is generally well-presented, although I also have some suggestions about this below.

In summary, I feel that this paper should be accepted to CMP, once the following points are addressed:

1. I think the authors' main result (Theorem 1) should be stated, possibly in simplified form, in the introduction. At present, one needs to wait until Section 8 on p18 to find out what it is... I also think that the result on BVPs should be stated in the introduction, especially as its complexity in terms of T is worse than Theorem 1. (There could also be a brief mention of why this parameter is worse.)
\end{lstlisting}

We added an informal statement of Theorem 1 for IVPs and Theorem 2 for BVPs in the introduction (pages 3--4). We also added a discussion of why the complexity is higher for BVPs.

\begin{lstlisting}
2. p2, line 51: it seems unusual to me to claim that epsilon^{1/k} is "polynomial in 1/epsilon" for k > 1.
\end{lstlisting}

The claim here is that $\epsilon^{-1/k}=(1/\epsilon)^{1/k}$ is polynomial in $1/\epsilon$, i.e., grows as a fixed power of $1/\epsilon$.

\begin{lstlisting}
3. I think it should be clarified somewhere in the introduction that, if one wishes to extract classical information from the state produced as the solution to the system of ODEs (for example by estimating the acceptance probability of a measurement) the complexity will usually be poly(1/epsilon), so the exponential improvement in scaling with epsilon achieved by the authors' algorithm does not necessarily correspond to an exponential runtime improvement in the context of a larger algorithm.
\end{lstlisting}

We clarified this point at the top of page 3.

\begin{lstlisting}
4. p4, sec 2: I think it's confusing to discuss interpolation nodes in the range [-1,1] (rather than [0,T]) without previously mentioning that you will be rescaling t to fit into this window. I suggest adding a mention of this rescaling beforehand.
\end{lstlisting}

We added a discussion of this rescaling after equation (2.3).

\begin{lstlisting}
5. p6, line 26: "can dramatically increase the spectral norm": it seems that the spectral norm would only increase by a factor of O(T), which would still give an algorithm which runs in time polynomial in T. I suggest being more precise and saying that you are looking for an algorithm that runs in time linear in T.
\end{lstlisting}

We removed the word ``dramatically'', explained that we do this ``to reduce the dependence on $T$'', and mentioned that this choice lets us give an algorithm with complexity close to linear in $T$.

\begin{lstlisting}
6. A related, general point: the authors' algorithm is based on dividing the time window [0,T] into small (constant size) intervals, and then introducing a condition in the overall linear system that enforces the starting state of interval i+1 to be the same as the ending state of interval i. It seemed to me that it might be possible to employ a simpler approach, where one just solves the ODE problem for each interval individually, and uses the state output from interval i as the initial condition for interval i+1. This would involve m steps, but given that the condition number of the overall linear system scales linearly with m, it seems possible that this would lead to a similar overall complexity.
\end{lstlisting}

While such an approach works well for unitary dynamics, it does not necessarily give an algorithm with good complexity for more general differential equations. If the output state of one step of the evolution is only produced with some constant success probability, then the need to successfully prepare the correct input state at every stage will result in an overhead exponential in the number of intervals. Using a single copy of the input state, the success probability cannot be boosted to be arbitrarily close to 1, even in principle, if the dynamics are non-unitary.

\begin{lstlisting}
7. p9, Lemma 3: I think that kappa_V, defined in the statement of the lemma, does not actually appear in the lemma's proof.
\end{lstlisting}

We deleted some unnecessary hypotheses (including the definition of $\kappa_V$) in Lemma 3.

\begin{lstlisting}
8. p10, eqn (4.16): C appears in a couple of lines, but then disappears - should this be deleted?
\end{lstlisting}

This was simply a typo. We have removed a few occurrences of $C$ in (4.16).

\begin{lstlisting}
9. p10, line 59-60: in the last sentence, it doesn't seem quite right to me to call this "the quantum spectral method". Isn't this analysis just proving the accuracy of a discretization approach for solving a system of ODEs? (That is, at this point, the method isn't inherently "quantum" - the linear system could still be solved by a classical solver.)
\end{lstlisting}

We replaced ``the quantum spectral method" by  ``the linear system".

\begin{lstlisting}
10. p12, eqn (5.4): I'm not sure the last inequality is correct. It seems to me that considering l=0, every summand is equal to 1, so we get a total of 3(n+1)/2. (This doesn't significantly affect the final bound.)
\end{lstlisting}

Thank you for pointing out this issue, which only affected the case $l=0$. We changed the argument to prove (5.4), giving an upper bound of $n+1$. This still implies the desired upper bound in (5.5), so it does not affect the final bound.

\begin{lstlisting}
11. p12, eqn. (5.11): isn't there the exact bound ||L_3|| = sqrt{n+1}, rather than <= n+1? (L_3 is the tensor product of an identity matrix and a rank-1 matrix.) Again, I don't think this affects the final result much, but this and the previous point do change eq (5.14)
\end{lstlisting}

Thank you for pointing this out. We have updated (5.11) to give the exact expression. We also propagated this change to (5.14). The final upper bound of $(n+1)^3$ remains the same.

\begin{lstlisting}
12. p12, eqn (5.12): should this actually be an inequality (<=2), rather than an equality?
\end{lstlisting}

We changed ``$=$" $\to$ ``$\le$" in (5.12).

\begin{lstlisting}
13. p13,15, eqns (5.17), (5.18), (5.37), (5.38): should T_h be Gamma_h in these equations?
\end{lstlisting}

Thanks for catching these typos. We changed ``$T_h$" $\to$ ``$\Gamma_h$" in (5.17), (5.18), (5.35), (5.36), (5.37), (5.38). We also made this correction in (6.11) and (6.12).

\begin{lstlisting}
14. p17, Lemma 6: "for and" -> "for all"?
\end{lstlisting}

We changed ``for and" $\to$ ``for any".

\begin{lstlisting}
15. p18, line 57: "and" -> ", by"?
\end{lstlisting}

We changed ``and" $\to$ ``, by".

\begin{lstlisting}
16. p27: I think that in a number of places on this page, n should be N.
\end{lstlisting}

This has been fixed.

\end{document}
